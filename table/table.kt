package table

import java.io.BufferedReader
import java.io.FileReader

fun main() {
  var caseCount = -1
  val reader = BufferedReader(FileReader("input.txt"))

  var caseIndex = 0

  for (line in reader.lineSequence()) {
    if (caseCount == -1) {
      caseCount = line.toInt()
      continue
    }

    val caseParams = line.split(" ").map { it.toInt() }
    val case = Case(
      caseParams[0],
      caseParams[1],
      caseParams[2],
      caseParams[3]
    )

    println(case.solve())

    caseIndex++
    if (caseIndex == caseCount) {
      break
    }
  }
}

data class Case(
  val tableLength: Int,
  val points: Int,
  val longs: Int,
  val lechons: Int
)

fun Case.solve(): Int {
  // Get the total area of the table.
  val totalArea = this.tableLength * 2

  // Get the total blank area = number of blank cells.
  val coveredArea = this.points + this.longs * 2 + this.lechons * 3
  val blanks = totalArea - coveredArea

  val blocks = mutableListOf<BlockType>()
  repeat(this.points) { blocks.add(BlockType.Point) }
  repeat(this.longs) { blocks.add(BlockType.Long) }
  repeat(this.lechons) { blocks.add(BlockType.Lechon) }
  repeat(blanks) { blocks.add(BlockType.Blank) }
  val blocksIndexed = blocks
    .withIndex()
    .map({ IndexedBlockType(it.value, it.index) })
    .toList()

  // Permute between points, longs, lechons, and blanks
  val validPermutationCount = blocksIndexed.permute()
    .map { this.getOrientCombinationCount(it) }
    // Count permutations.
    .sum()

  return validPermutationCount
}

fun Case.getOrientCombinationCount(permutation: List<IndexedBlockType>): Int {
  // For each block in the permutation,
  // choose whether to orient it horizontally or vertically.

  return orientationCombinations(permutation)
    .map {
      // Check if the blocks permuted in `permutation` way
      // oriented in `combination` way
      // would be a valid configuration of blocks in the grid.

      // Place the blocks in the grid.
      // Assuming 2 is the vertical width and
      // tableLength is the horizontal length.
      val grid = Grid(this.tableLength)

      var coord = 0
      var col: Int
      var row: Int
      for ((indexedBlockType, orientation) in permutation.zip(it)) {

        coordLoop@ do {
          col = coord % this.tableLength
          row = coord / this.tableLength

          val block = indexedBlockType.orient(orientation)

          if (row > 1) {
            break
          }
          if (grid.rows[row][col] != null) {
            coord += 1
            continue
          }

          if (grid.place(coord, block)) {
            coord += 1
            break
          }
          else {
            coord += 1
          }
        } while (true)
      }
      // Pass if all the blocks have been placed
      return@map (grid.placedSize == permutation.size)
    }
  // Count valid.
  .map({ if (it) 1 else 0 })
  .sum()
}

fun orientationCombinations(blocks: List<IndexedBlockType>): List<List<Orientation>> {
  val combinations = mutableListOf<List<Orientation>>()
  val allOrientations = listOf(Orientation.Horizontal, Orientation.Vertical)

  val getCombinationsForBlockType = {
    block: IndexedBlockType ->
      when (block.blockType) {
        BlockType.Long -> allOrientations
        else -> listOf(Orientation.Horizontal)
      }
  }

  val stack = getCombinationsForBlockType(blocks[0])
    .map({ it -> listOf(it) })
    .toMutableList()

  while (stack.size != 0) {
    val params = stack.pop()
    if (params.size < blocks.size) {
      for (orientation in getCombinationsForBlockType(blocks[params.size])) {
        stack.add(listOf(*params.toTypedArray(), orientation))
      }
    }
    else {
      combinations.add(params)
    }
  }

  return combinations.toList()
}

fun <T> List<T>.permute(): List<List<T>> {
  var ret = mutableListOf<List<T>>()
  var c = repeatItems(0, this.size).toMutableList()

  var arr = this
  ret.add(arr)

  var i = 0
  while (i < this.size) {
    if (c[i] < i) {
      if (i % 2 == 0) {
        arr = arr.swappedAt(0, i)
      }
      else {
        arr = arr.swappedAt(c[i], i)
      }
      ret.add(arr)
      c[i] += 1
      i = 0
    }
    else {
      c[i] = 0
      i += 1
    }
  }

  return ret.toList()
}

fun <T> MutableList<T>.swapAt(i: Int, j: Int) {
  val tmp = this[j]
  this[j] = this[i]
  this[i] = tmp
}

fun <T> List<T>.swappedAt(i: Int, j: Int): List<T> {
  val copy = this.toMutableList()
  copy.swapAt(i, j)
  return copy.toList()
}

fun <T> MutableList<T>.reverseAtRange(range: IntRange) {
  val sliced = this.slice(range)
  for ((indexInSlice, reversedIndex) in range.reversed().withIndex()) {
    this[reversedIndex] = sliced[indexInSlice]
  }
}

class Grid(val tableLength: Int) {

  val rows = (0..1)
    .map { (0 until tableLength).map{ null as Block? }.toMutableList() }
  var placedSize = 0
    private set

  fun place(coord: Int, block: Block): Boolean {
    val row = coord / tableLength
    val col = coord % tableLength
    for (i in 0 until block.blockType.length) {
      val (rowDelta, colDelta) = when (block.orientation) {
        Orientation.Horizontal -> 0 to i
        Orientation.Vertical -> i to 0
      }
      // Range checks
      if (row + rowDelta !in this.rows.indices) {
        return false
      }
      if (col + colDelta !in 0 until this.tableLength) {
        return false
      }

      if (this.rows[row + rowDelta][col + colDelta] != null) {
        return false // Occupied
      }
      else {
        this.rows[row + rowDelta][col + colDelta] = block
      }
    }
    this.placedSize += 1
    return true
  }

  fun debugPrint() {
    println("grid:")
    for (row in rows) {
      for (cell in row) {
        print("${cell.debugPrint()} ")
      }
      println()
    }
  }
}

data class IndexedBlockType(
  val blockType: BlockType,
  val index: Int
)

fun IndexedBlockType.orient(orientation: Orientation) =
  Block(this.blockType, orientation, this.index)

fun IndexedBlockType.debugPrint(): String =
  "${this.index}${this.blockType.display}"

data class Block(
  val blockType: BlockType,
  val orientation: Orientation,
  val index: Int
)

fun Block?.debugPrint(): String {
  if (this == null) {
    return "..."
  }
  val orient = when (this.orientation) {
    Orientation.Horizontal -> "H"
    Orientation.Vertical -> "V"
  }
  return "${this.index}${this.blockType.display}${orient}"
}

enum class BlockType(val length: Int, val display: String) {
  //Unassigned(0, "."),
  Blank(0, "-"),
  Point(1, "."),
  Long(2, "!"),
  Lechon(3, "=")
}

enum class Orientation(val x: Int) {
  Horizontal(0),
  Vertical(1)
}

fun <T> MutableList<T>.pop(): T {
  if (this.size == 0) {
    throw IndexOutOfBoundsException()
  }
  val last = this.last()
  this.removeAt(this.lastIndex)
  return last
}

fun <T> repeatItems(item: T, n: Int)=(0 until n).map { item }.toList()

fun <T> generateItems(fn: () -> T, n: Int)= (0 until n).map { fn() }.toList()
