const FS = require("fs")
const Readline = require("readline")

async function run() {
  const rl = Readline.createInterface({
    input: FS.createReadStream(`${__dirname}/input.txt`),
    crlfDelay: Infinity
  })

  let isFirstLineRead
  let colCount
  let rowCount
  let rows = []

  for await (const line of rl) {
    if (isFirstLineRead) {
      rows.push(line.split(" "))
      rowCount--
      if (rowCount === 0) {
        break
      }
    } else {
      ;[rowCount, colCount] = line.split(" ").map(s => Number.parseInt(s))
      isFirstLineRead = true
    }
  }

  const minimumPath = getMinimumPathForGrid(rows, colCount)

  process.stdout.write(
    `Optimum cost is ${sum(minimumPath)} via ${minimumPath.join(" ")}\n`
  )
}

function getMinimumPathForGrid(rows, colCount) {
  const rowCount = rows.length
  const minimumPathsForEntry = []
  for (let i = 0; i < rowCount; i++) {
    minimumPathsForEntry.push(getMinimumPath(rows, colCount, [[i, 0]]))
  }
  return min(minimumPathsForEntry)
}

function getMinimumPath(rows, colCount, path) {
  const lastCell = last(path)
  const rowCount = rows.length
  if (lastCell[1] === colCount - 1) {
    return path
  } else {
    const paths = []
    for (let rowDelta = -1; rowDelta <= 1; rowDelta++) {
      const newRow = lastCell[0]
      if (newRow < 0 || newRow >= rowCount) {
        continue
      }
      const newPath = [...path, [newRow + rowDelta, lastCell[1] + 1]]
      paths.push(getMinimumPath(rows, colCount, newPath))
    }
    return min(paths, (path1, path2) => {
      const cells1 = getCells(rows, path1)
      const cells2 = getCells(rows, path2)
      return comparePaths(cells1, cells2)
    })
  }
}

function getCells(rows, path) {
  return path.map(node => rows[node[0]][node[1]])
}

function min(arr, compareFn) {
  return arr.reduce((a, b) => compareFn(a, b))
}

function comparePaths(path1, path2) {
  const sum1 = sum(getCells(path1))
  const sum2 = sum(getCells(path2))
  return sum1 - sum2
}

function sum(nums) {
  return nums.reduce((a, b) => a + b, 0)
}

function last(arr) {
  return arr[arr.length - 1]
}

if (require.main === module) {
  ;(async () => {
    try {
      run()
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })()
}
