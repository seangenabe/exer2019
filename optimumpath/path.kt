package optimumpath;

import java.io.BufferedReader;
import java.io.FileReader;

fun main() {
  OptimumPath()
}

class OptimumPath {

  val rows: MutableList<List<Int>> = mutableListOf()
  var rowCount = 0
  var colCount = 0

  constructor() {
    val reader = BufferedReader(FileReader("input.txt"))

    var isFirstLineRead = false
    var rowIndex = 0

    for (line in reader.lineSequence()) {
      if (isFirstLineRead) {
        val nums = line.split(" ").map { it.toInt() }
        this.rows.add(nums)
        rowIndex++
        if (rowIndex == this.rowCount) {
          break
        }
      }
      else {
        val split = line.split(" ").map { it.toInt() }
        this.rowCount = split[0]
        this.colCount = split[1]
        isFirstLineRead = true
      }
    }

    val minimumPath = this.getMinimumPath()

    println("Optimum cost is ${this.getPathCost(minimumPath)} via ${minimumPath.map { it + 1 }.joinToString(" ")}")
  }

  fun getMinimumPath(): IntArray {
    var minimumPath: IntArray? = null
    var minimumCost: Int = Int.MAX_VALUE
    for (startingRow in 0 until this.rowCount) {
      val indexStack = mutableListOf(intArrayOf(startingRow))
      while (indexStack.size != 0) {
        val path = indexStack.pop()
        if (path.size < this.colCount) {
          val lastNode = path.last()
          for (rowDelta in -1..1) {
            val newRowIndex = lastNode + rowDelta
            // Disregard out-of-bounds paths
            if (!(0 until this.rowCount).contains(newRowIndex)) {
              continue
            }
            indexStack.add(intArrayOf(*path, newRowIndex))
          }
        }
        else {
          // Get the smaller path
          val cost = this.getPathCost(path)
          if (cost < minimumCost) {
            minimumPath = path
            minimumCost = cost
          }
        }
      }
    }
    return minimumPath!!
  }

  fun getPathCost(path: IntArray): Int {
    return path.foldIndexed(
      0,
      { i, a, b -> this.rows[b][i] + a }
    )
  }

  fun <T> MutableList<T>.pop(): T {
    if (this.size == 0) {
      throw IndexOutOfBoundsException()
    }
    val last = this.last()
    this.removeAt(this.lastIndex)
    return last
  }

}
