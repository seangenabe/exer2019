const FS = require("fs")
const Readline = require("readline")

class OptimumPath {
  async run() {
    const rl = Readline.createInterface({
      input: FS.createReadStream(`${__dirname}/input2.txt`),
      crlfDelay: Infinity
    })

    let isFirstLineRead
    let rowIndex = 0
    this.rows = []

    for await (const line of rl) {
      if (isFirstLineRead) {
        this.rows.push(line.split(" ").map(s => Number.parseInt(s)))
        rowIndex++
        if (rowIndex === this.rowCount) {
          break
        }
      } else {
        const [rowCount, colCount] = line
          .split(" ")
          .map(s => Number.parseInt(s))
        this.rowCount = rowCount
        this.colCount = colCount
        isFirstLineRead = true
      }
    }

    const minimumPath = this.getMinimumPath()

    process.stdout.write(
      `Optimum cost is ${this.getPathCost(minimumPath)} via ${minimumPath
        .map(x => x + 1)
        .join(" ")}\n`
    )
  }

  getMinimumPath() {
    let minimumPath = null
    let minimumCost = Infinity
    for (let startingRow = 0; startingRow < this.rowCount; startingRow++) {
      const indexStack = [[startingRow]]
      while (indexStack.length !== 0) {
        const path = indexStack.pop()
        if (path.length < this.colCount) {
          const lastNode = lastItem(path)
          for (let rowDelta = -1; rowDelta <= 1; rowDelta++) {
            const newRowIndex = lastNode + rowDelta
            // Disregard out-of-bounds paths
            if (newRowIndex < 0 || newRowIndex >= this.rowCount) {
              continue
            }
            indexStack.push([...path, newRowIndex])
          }
        } else {
          // Get the smaller path
          const cost = this.getPathCost(path)
          if (cost < minimumCost) {
            minimumPath = path
            minimumCost = cost
          }
        }
      }
    }
    return minimumPath
  }

  getPathCost(path) {
    return path.reduce((a, b, bIndex) => this.rows[b][bIndex] + a, 0)
  }
}

function lastItem(arr) {
  return arr[arr.length - 1]
}

if (require.main === module) {
  ;(async () => {
    try {
      new OptimumPath().run()
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })()
}
